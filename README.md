# Gryff Hash Class

Includes interfaces for creating hashes.

Currently supports hashing of JS object and files.

## Installation

npm install --save @gryffinc/hasher